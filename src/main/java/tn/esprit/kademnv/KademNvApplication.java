package tn.esprit.kademnv;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KademNvApplication {

	public static void main(String[] args) {
		SpringApplication.run(KademNvApplication.class, args);
	}

}
